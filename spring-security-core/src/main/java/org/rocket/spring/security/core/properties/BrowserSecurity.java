package org.rocket.spring.security.core.properties;

import org.rocket.spring.security.core.enums.LoginTypeEnum;

/**
 * @author he peng
 * @create 2018/1/23 13:51
 * @see
 */
public class BrowserSecurity {

    private String loginPage = "/my-login.html";

    private LoginTypeEnum loginType = LoginTypeEnum.JSON;

    public LoginTypeEnum getLoginType() {
        return loginType;
    }

    public BrowserSecurity setLoginType(LoginTypeEnum loginType) {
        this.loginType = loginType;
        return this;
    }

    public String getLoginPage() {
        return loginPage;
    }

    public BrowserSecurity setLoginPage(String loginPage) {
        this.loginPage = loginPage;
        return this;
    }

    @Override
    public String toString() {
        return "BrowserSecurity{" +
                "loginPage='" + loginPage + '\'' +
                ", loginType=" + loginType +
                '}';
    }
}
