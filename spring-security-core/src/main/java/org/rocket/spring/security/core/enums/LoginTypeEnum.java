package org.rocket.spring.security.core.enums;

/**
 * @author He Peng
 * @create 2018-01-23 22:07
 * @update 2018-01-23 22:07
 * @updatedesc : 更新说明
 * @see
 */
public enum LoginTypeEnum {

    REDIRECT,

    JSON;
}
