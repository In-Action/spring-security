package org.rocket.spring.security.core.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author he peng
 * @create 2018/1/23 13:50
 * @see
 */

@ConfigurationProperties(prefix = "my.security")
public class SecurityProperties {

    private BrowserSecurity browser = new BrowserSecurity();

    public BrowserSecurity getBrowser() {
        return browser;
    }

    public SecurityProperties setBrowser(BrowserSecurity browser) {
        this.browser = browser;
        return this;
    }

    @Override
    public String toString() {
        return "SecurityProperties{" +
                "browser=" + browser +
                '}';
    }
}
