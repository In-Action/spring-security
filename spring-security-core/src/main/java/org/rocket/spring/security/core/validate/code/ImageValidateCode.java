package org.rocket.spring.security.core.validate.code;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

/**
 * @author He Peng
 * @create 2018-01-23 22:29
 * @update 2018-01-23 22:29
 * @updatedesc : 更新说明
 * @see
 */
public class ImageValidateCode {

    private BufferedImage image;

    private String code;

    private LocalDateTime expireTime;

    public ImageValidateCode(BufferedImage image, String code, LocalDateTime expireTime) {
        this.image = image;
        this.code = code;
        this.expireTime = expireTime;
    }

    public ImageValidateCode(BufferedImage image, String code , long expireSecond) {
        this(image , code , LocalDateTime.now().plusSeconds(expireSecond));
    }

    public BufferedImage getImage() {
        return image;
    }

    public ImageValidateCode setImage(BufferedImage image) {
        this.image = image;
        return this;
    }

    public String getCode() {
        return code;
    }

    public ImageValidateCode setCode(String code) {
        this.code = code;
        return this;
    }

    public LocalDateTime getExpireTime() {
        return expireTime;
    }

    public ImageValidateCode setExpireTime(LocalDateTime expireTime) {
        this.expireTime = expireTime;
        return this;
    }

    @Override
    public String toString() {
        return "ImageValidateCode{" +
                "image=" + image +
                ", code='" + code + '\'' +
                ", expireTime=" + expireTime +
                '}';
    }
}
