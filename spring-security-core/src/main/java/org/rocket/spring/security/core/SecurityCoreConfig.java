package org.rocket.spring.security.core;

import org.rocket.spring.security.core.properties.SecurityProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author He Peng
 * @create 2018-01-23 21:38
 * @update 2018-01-23 21:38
 * @updatedesc : 更新说明
 * @see
 */

@Configuration
@EnableConfigurationProperties(SecurityProperties.class)
public class SecurityCoreConfig {
}
