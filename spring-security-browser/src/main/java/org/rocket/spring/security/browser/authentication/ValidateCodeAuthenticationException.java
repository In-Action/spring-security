package org.rocket.spring.security.browser.authentication;

import org.springframework.security.core.AuthenticationException;

/**
 * @author he peng
 * @create 2018/1/25 12:49
 * @see
 */
public class ValidateCodeAuthenticationException extends AuthenticationException {

    public ValidateCodeAuthenticationException(String msg, Throwable t) {
        super(msg, t);
    }

    public ValidateCodeAuthenticationException(String msg) {
        super(msg);
    }
}
