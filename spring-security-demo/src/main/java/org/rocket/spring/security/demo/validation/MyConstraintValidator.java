package org.rocket.spring.security.demo.validation;

import org.rocket.spring.security.demo.validation.annotation.MyConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * @author He Peng
 * @create 2018-01-21 15:03
 * @update 2018-01-21 15:03
 * @updatedesc : 更新说明
 * @see
 */
public class MyConstraintValidator
        implements ConstraintValidator<MyConstraint, Object> {

    @Override
    public void initialize(MyConstraint constraintAnnotation) {
        System.out.println("MyConstraintValidator initialize");
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (Objects.equals(value , "hello")) {
            return false;
        }
        return true;
    }
}
