package org.rocket.spring.security.demo.web.async;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

/**
 * @author He Peng
 * @create 2018-01-21 22:37
 * @update 2018-01-21 22:37
 * @updatedesc : 更新说明
 * @see
 */

@RestController
public class AsyncController {

    private Logger LOG = LoggerFactory.getLogger(AsyncController.class);

    @Autowired
    private MockMessageQueue mockMessageQueue;

    @Autowired
    private DeferredResultHolder deferredResultHolder;

    @RequestMapping(path = "/order")
    public DeferredResult<String> order() {
        LOG.info("处理请求的主线程开始 ===>");
        String orderNumber = RandomStringUtils.randomNumeric(8);
        this.mockMessageQueue.setPlaceOrder(orderNumber);
        DeferredResult<String> deferredResult = new DeferredResult<>();
        this.deferredResultHolder.getDeferredResultMap().put(orderNumber , deferredResult);
        LOG.info("处理请求的主线程返回 ===>");

        return deferredResult;
    }
}
