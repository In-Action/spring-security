package org.rocket.spring.security.demo.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author He Peng
 * @create 2018-01-20 22:31
 * @update 2018-01-20 22:31
 * @updatedesc : 更新说明
 * @see
 */

@SpringBootApplication
@ComponentScan(basePackages = "org.rocket.spring.security")
@EnableSwagger2
public class SpringSecurityDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityDemoApplication.class , args);
    }
}
