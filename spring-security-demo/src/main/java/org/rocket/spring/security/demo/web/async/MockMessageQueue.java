package org.rocket.spring.security.demo.web.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author He Peng
 * @create 2018-01-21 22:39
 * @update 2018-01-21 22:39
 * @updatedesc : 更新说明
 * @see
 */

@Component
public class MockMessageQueue {

    private Logger LOG = LoggerFactory.getLogger(MockMessageQueue.class);

    private String placeOrder;
    private String completeOrder;

    public String getPlaceOrder() {
        return placeOrder;
    }

    public MockMessageQueue setPlaceOrder(String placeOrder) {
        new Thread(() -> {
            LOG.info("接到下单请求 ==> {} " , placeOrder);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.placeOrder = placeOrder;
            this.completeOrder = placeOrder;
            LOG.info("下单请求处理完成 ==> {} " , placeOrder);
        } , "OrderHandle-Thread").start();
        return this;
    }

    public String getCompleteOrder() {
        return completeOrder;
    }

    public MockMessageQueue setCompleteOrder(String completeOrder) {
        this.completeOrder = completeOrder;
        return this;
    }

    @Override
    public String toString() {
        return "MockMessageQueue{" +
                "placeOrder='" + placeOrder + '\'' +
                ", completeOrder='" + completeOrder + '\'' +
                '}';
    }
}
