package org.rocket.spring.security.demo.dto;

import org.rocket.spring.security.demo.validation.annotation.MyConstraint;

/**
 * @author He Peng
 * @create 2018-01-21 11:24
 * @update 2018-01-21 11:24
 * @updatedesc : 更新说明
 * @see
 */
public class UserDTO {

    @MyConstraint(message = "自定义校验器测试")
    private String username;

    private String password;

    public String getUsername() {
        return username;
    }

    public UserDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserDTO setPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    public String toString() {
        return "UserVO{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
