package org.rocket.spring.security.demo.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author He Peng
 * @create 2018-01-22 1:14
 * @update 2018-01-22 1:14
 * @updatedesc : 更新说明
 * @see
 */

@RestController
public class AuthenticationController {

    @GetMapping(path = "/basic")
    public ResponseEntity<String> httpBasicAuthentication() {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized");
    }
}
