package org.rocket.spring.security.demo.web.async;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author He Peng
 * @create 2018-01-21 22:42
 * @update 2018-01-21 22:42
 * @updatedesc : 更新说明
 * @see
 */

@Component
public class DeferredResultHolder {

    private Map<String , DeferredResult<String>> deferredResultMap = new ConcurrentHashMap<>();

    public Map<String, DeferredResult<String>> getDeferredResultMap() {
        return deferredResultMap;
    }

    public DeferredResultHolder setDeferredResultMap(Map<String, DeferredResult<String>> deferredResultMap) {
        this.deferredResultMap = deferredResultMap;
        return this;
    }

    @Override
    public String toString() {
        return "DeferredResultHolder{" +
                "deferredResultMap=" + deferredResultMap +
                '}';
    }
}
