package org.rocket.spring.security.demo.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.rocket.spring.security.demo.boot.SpringSecurityDemoApplication;
import org.rocket.spring.security.demo.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.*;
/**
 * @author He Peng
 * @create 2018-01-21 11:17
 * @update 2018-01-21 11:17
 * @updatedesc : 更新说明
 * @see
 */

/**
 *
 * @author He Peng
 * @create 2018-01-21 11:17
 * @update 2018-01-21 11:17
 * @updateDesc : 更新说明
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringSecurityDemoApplication.class)
public class UserControllerTest {


    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void whenQuerySuccess() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/user")
                    .contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(MockMvcResultMatchers.status().isOk())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(3));
    }

    @Test
    public void create() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("hello").setPassword("dsewff");
        ObjectMapper objectMapper = new ObjectMapper();
        String content = this.mockMvc.perform(MockMvcRequestBuilders.post("/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(userDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println("content : " + content);
    }

    @Test
    public void whenGenInfoSuccess() throws Exception {

    }
}